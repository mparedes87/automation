#!/bin/bash

wget  https://www.zombodb.com/releases/v5.6.16-1.0.18/zombodb_jessie_pg95-5.6.16-1.0.18_amd64.deb

dpkg -i zombodb_jessie_pg95-5.6.16-1.0.18_amd64.deb

rm -rf  zombodb_jessie_pg95-5.6.16-1.0.18_amd64.deb

sudo -u postgres psql -c "CREATE EXTENSION zombodb;"

sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install https://www.zombodb.com/releases/v5.6.16-1.0.18/zombodb-es-plugin-5.6.16-1.0.18.zip
