#!/bin/bash

VERSION=5.6.16;

##############################################################################
################                      ELASTICSEARCH
##############################################################################

wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-${VERSION}.deb
sudo  dpkg -i elasticsearch-${VERSION}.deb 
rm -rf elasticsearch-${VERSION}.deb
sed -ri  's/\#network\.host\: 192.168.0.1/network\.host\: 0.0.0.0/g; s/#http.port: 9200/http.port: 9200/g;'   /etc/elasticsearch/elasticsearch.yml

sudo  /etc/init.d/elasticsearch start

sleep 30

sudo  /etc/init.d/elasticsearch status

##############################################################################
################                       KIBANA
##############################################################################

wget https://artifacts.elastic.co/downloads/kibana/kibana-${VERSION}-amd64.deb
sudo  dpkg -i kibana-${VERSION}-amd64.deb
rm -rf kibana-${VERSION}-amd64.deb

#sudo   /usr/share/kibana/bin/kibana-plugin  install https://artifacts.elastic.co/downloads/packs/x-pack/x-pack-5.6.16.zip

sed -ri  's/\#elasticsearch.url/elasticsearch.url/g; s/#server.host: "localhost"/server.host: "0.0.0.0"/g; s/#server.port: 5601/server.port: 5601/g;'   /etc/kibana/kibana.yml

sudo  /etc/init.d/kibana start

sleep 30

sudo  /etc/init.d/kibana status