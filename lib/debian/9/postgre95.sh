#!/bin/bash
apt-get update
apt-get -y install wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list

apt-get update

apt-get -y install postgresql-9.5


USER="vm"
PASS="qwerty"
DB="vm"


sudo -u postgres createuser ${USER}
sudo -u postgres createdb ${DB}
sudo -u postgres psql -c "grant all privileges on database ${DB} to ${USER};"
sudo -u postgres psql -c "alter user ${USER} with encrypted password '${PASS}';"

echo "host  all  all  all  md5" >> /etc/postgresql/9.5/main/pg_hba.conf
sudo sed -ri "s/#(listen_addresses\s*\=\s*) / \1 '*' /g;  s/'localhost'//g; "  /etc/postgresql/9.5/main/postgresql.conf

sudo /etc/init.d/postgresql restart