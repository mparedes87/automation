#!/bin/bash

sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y postgresql-9.6 postgresql-contrib-9.6

USER="local"
PASS="qwerty"
DB="test"


sudo -u postgres createuser ${USER}
sudo -u postgres createdb ${DB}
sudo -u postgres psql -c "grant all privileges on database ${DB} to ${USER};"
sudo -u postgres psql -c "alter user ${USER} with encrypted password '${PASS}';"

echo "host  all  all  all  md5" >> /etc/postgresql/9.6/main/pg_hba.conf
sudo sed -ri "s/#(listen_addresses\s*\=\s*) / \1 '*' /g;  s/'localhost'//g; "  /etc/postgresql/9.6/main/postgresql.conf

sudo /etc/init.d/postgresql restart